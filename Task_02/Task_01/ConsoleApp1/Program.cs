﻿using System;
using System.Text;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.WriteLine("Лабораторна робота №1.\nВиконав: Зайцев О. І., група ІПЗ-22-4\nВаріант №1\nЗавдання 2.");
            double a, b, c;
            do
            {
                Console.Write("Введіть значення a = "); if (double.TryParse(Console.ReadLine(), out a)) break;
                else
                {
                    Console.WriteLine("Помилка");
                }
            }
            while (true);
            do
            {
                Console.Write("Введіть значення b = ");
                if (double.TryParse(Console.ReadLine(), out b)) break;
                else { Console.WriteLine("Помилка"); }
            }
            while (true);
            do
            {
                Console.Write("Введіть значення c = ");
                if (double.TryParse(Console.ReadLine(), out c)) break;
                else
                {
                    Console.WriteLine("Помилка");
                }
            } while (true);
            double d = b * b - 4 * a * c;
            Console.WriteLine("Дискримінант = " + d);
            if(a == 0)
            {
                double x = -c / b;
                Console.WriteLine("x = " + x);
            }
            else if (d == 0)
            {
                double x = (-b) / (2 * a);
                Console.WriteLine($"x = {x:F3}");
            }
            else if(d > 0)
            {
                double x1 = (Math.Sqrt(d) - b) / (2 * a);
                double x2 = (-Math.Sqrt(d) - b) / (2 * a);
                Console.WriteLine($"x1 = {x1:F3}");
                Console.WriteLine($"x2 = {x2:F3}");
            }
            else if(d < 0)
            {
                Console.WriteLine("Коренів немає");
            }
        }
    }
}
