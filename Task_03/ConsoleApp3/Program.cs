﻿using System;
using System.Text;

namespace ConsoleApp3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.WriteLine("Лабораторна робота №1.\nВиконав: Зайцев О. І., група ІПЗ-22-4\nВаріант №1\nЗавдання 3.");
            double N, sum = 0;
            do
            {
                Console.Write("Введіть значення N = "); if (double.TryParse(Console.ReadLine(), out N)) break;
                else
                {
                    Console.WriteLine("Помилка");
                }
            }
            while (true);
            double m = 1;
            while(m <= N)
            {
                sum += Math.Pow(m, N - m + 1);
                m++;
            }
            Console.WriteLine($"Результат: {sum}");
        }
    }
}