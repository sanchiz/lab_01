﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x, y, z, s;
            bool ok;
            ok = double.TryParse(xInput.Text, out x);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення x!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse(yInput.Text, out y);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення y!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse(zInput.Text, out z);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення z!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            s = (2 * Math.Cos(x * x) - 0.5) / (0.5 + Math.Sin(Math.Pow(y, 2 - z))) + (z * z) / (7 - z / 3);
            Result.Text = $"{s:F3}";
        }
    }
}
