﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double a, b, c;
            bool ok;
            ok = double.TryParse(aInput.Text, out a);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse(bInput.Text, out b);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse(cInput.Text, out c);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення c!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double d = b * b - 4 * a * c;
            Descriminant.Text = $"{d:F3}";
            Console.WriteLine("Дискримінант = " + d);
            if (a == 0)
            {
                double x = -c / b;
                x1.Visibility = Visibility.Visible;
                x1Res.Visibility = Visibility.Visible;
                x2.Visibility = Visibility.Hidden;
                x2Res.Visibility = Visibility.Hidden;
                x1Res.Text = $"{x:F3}";
                x1.Content = "x =";
            }
            else if (d == 0)
            {
                double x = (-b) / (2 * a);
                x1.Visibility = Visibility.Visible;
                x1Res.Visibility = Visibility.Visible;
                x2.Visibility = Visibility.Hidden;
                x2Res.Visibility = Visibility.Hidden;
                x1Res.Text = $"{x:F3}";
                x1.Content = "x =";
            }
            else if (d > 0)
            {
                double x_1 = (Math.Sqrt(d) - b) / (2 * a);
                double x_2 = (-Math.Sqrt(d) - b) / (2 * a);
                x1.Visibility = Visibility.Visible;
                x1Res.Visibility = Visibility.Visible;
                x2.Visibility = Visibility.Visible;
                x2Res.Visibility = Visibility.Visible;
                x1Res.Text = $"{x_1:F3}";
                x2Res.Text = $"{x_2:F3}";
                Console.WriteLine($"x1 = {x1:F3}");
                Console.WriteLine($"x2 = {x2:F3}");
                x1.Content = "x1 =";
            }
            else if (d < 0)
            {
                x1.Visibility = Visibility.Hidden;
                x1Res.Visibility = Visibility.Hidden;
                x2.Visibility = Visibility.Hidden;
                x2Res.Visibility = Visibility.Hidden;
                MessageBox.Show("Дискримінант менше нуля!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
