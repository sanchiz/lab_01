﻿using System;
using System.Text;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.WriteLine("Лабораторна робота №1.\nВиконав: Зайцев О. І., група ІПЗ-22-4\nВаріант №1\nЗавдання 1.");
            double x, y, z, s;
            do
            {
                Console.Write("Введіть значення x = "); if (double.TryParse(Console.ReadLine(), out x)) break;
                else
                {
                    Console.WriteLine("Помилка");
                }
            }
            while (true);
            do
            {
                Console.Write("Введіть значення y = ");
                if (double.TryParse(Console.ReadLine(), out y)) break;
                else { Console.WriteLine("Помилка"); }
            }
            while (true);
            do
            {
                Console.Write("Введіть значення z = ");
                if (double.TryParse(Console.ReadLine(), out z)) break;
                else
                {
                    Console.WriteLine("Помилка");
                }
            } while (true);

            s = (2 * Math.Cos(x * x) - 0.5) / (0.5 + Math.Sin(Math.Pow(y, 2 - z))) + (z * z) / (7 - z / 3);
            Console.WriteLine($"Результат: {s:F3}");
        }
    }
}
