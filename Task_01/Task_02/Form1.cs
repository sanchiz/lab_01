﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Task_02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            double x, y, z, s;
            bool ok;
            ok = double.TryParse(xInput.Text, out x);
            if (!ok)
            {
                MessageBox.Show("Помилка введення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ok = double.TryParse(yInput.Text, out y);
            if (!ok)
            {
                MessageBox.Show("Помилка введення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ok = double.TryParse(zInput.Text, out z);
            if (!ok)
            {
                MessageBox.Show("Помилка введення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            s = (2 * Math.Cos(x * x) - 0.5) / (0.5 + Math.Sin(Math.Pow(y, 2 - z))) + (z * z) / (7 - z / 3);
            Result.Text = $"{s:F3}";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            this.Icon = new Icon("Resources/Zombie.ico");
        }
    }
}
