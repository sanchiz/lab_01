﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, EventArgs e)
        {
            double a, b, c;
            bool ok;
            ok = double.TryParse(aInput.Text, out a);
            if (!ok)
            {
                MessageBox.Show("Помилка введення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ok = double.TryParse(bInput.Text, out b);
            if (!ok)
            {
                MessageBox.Show("Помилка введення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            ok = double.TryParse(cInput.Text, out c);
            if (!ok)
            {
                MessageBox.Show("Помилка введення!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            double d = b * b - 4 * a * c;
            Descriminant.Text = $"{d:F3}";
            Console.WriteLine("Дискримінант = " + d);
            if (a == 0)
            {
                double x = -c / b;
                x1.Visible= true;
                x1Res.Visible = true;
                x2.Visible = false;
                x2Res.Visible = false;
                x1Res.Text = $"{x:F3}";
                x1.Text = "x =";
            }
            else if (d == 0)
            {
                double x = (-b) / (2 * a);
                x1.Visible = true;
                x1Res.Visible = true;
                x2.Visible = false;
                x2Res.Visible = false;
                x1Res.Text = $"{x:F3}";
                x1.Text = "x =";
            }
            else if (d > 0)
            {
                double x_1 = (Math.Sqrt(d) - b) / (2 * a);
                double x_2 = (-Math.Sqrt(d) - b) / (2 * a);
                x1.Visible = true;
                x1Res.Visible = true;
                x2.Visible = true;
                x2Res.Visible = true;
                x1Res.Text = $"{x_1:F3}";
                x2Res.Text = $"{x_2:F3}";
                Console.WriteLine($"x1 = {x1:F3}");
                Console.WriteLine($"x2 = {x2:F3}");
                x1.Text = "x1 =";
            }
            else if (d < 0)
            {
                x1.Visible = false;
                x1Res.Visible = false;
                x2.Visible = false;
                x2Res.Visible = false;
                MessageBox.Show("Дискримінант менше нуля", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            this.Icon = new Icon("Resources/Zombie.ico");
        }
    }
}
