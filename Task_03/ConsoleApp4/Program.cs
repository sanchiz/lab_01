﻿using System;
using System.Text;

namespace ConsoleApp4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.WriteLine("Лабораторна робота №1.\nВиконав: Зайцев О. І., група ІПЗ-22-4\nВаріант №1\nЗавдання 3.");
            double N, K, sum = 0;
            do
            {
                Console.Write("Введіть значення N = "); if (double.TryParse(Console.ReadLine(), out N)) break;
                else
                {
                    Console.WriteLine("Помилка");
                }
            }
            while (true);
            do
            {
                Console.Write("Введіть значення K = "); if (double.TryParse(Console.ReadLine(), out K)) break;
                else
                {
                    Console.WriteLine("Помилка");
                }
            }
            while (true);
            double m = 1;
            while (m <= N)
            {
                sum += Math.Pow(m, K);
                m++;
            }
            Console.WriteLine($"Результат: {sum}");
        }
    }
}