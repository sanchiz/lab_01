﻿using System;
using System.Text;

namespace ConsoleApp5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.WriteLine("Лабораторна робота №1.\nВиконав: Зайцев О. І., група ІПЗ-22-4\nВаріант №1\nЗавдання 3.");
            int q;
            int n = 1;
            int p = 0;
            int nep = 0;
            int pos = 0;
            int neg = 0;
            do
            {
                Console.Write("Введіть ціле число: ");
                if (int.TryParse(Console.ReadLine(), out n))
                {
                    if (n != 0)
                    {
                        if (n % 2 == 0)
                        {
                            p++;
                        }
                        else
                        {
                            nep++;
                        }

                        if (n > 0)
                        {
                            pos++;
                        }
                        else
                        {
                            neg++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("Помилка");
                }
            } while (true);
            Console.WriteLine("Кількість парних чисел: " + p);
            Console.WriteLine("Кількість непарних чисел: " + nep);
            Console.WriteLine("Кількість додатних чисел: " + pos);
            Console.WriteLine("Кількість від'ємних чисел: " + neg);
        }
    }
}